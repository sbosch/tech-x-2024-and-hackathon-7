import uvicorn
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI
from routes.vc_engine import create_vc_engine_router


# We create an instance of FastAPI using the application factory pattern
def provision_vc_toolkit() -> FastAPI:

    # Core routes
    vc_router = create_vc_engine_router()
    
    # Rules routes
    app = FastAPI(title="Gaia-x VC Toolkit",
                  description="**Gaia-x VC/VP ToolkitApplication** 🚀",
                  version="0.0.1",)
    app.include_router(vc_router)
    
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    return app


vc_toolkit = provision_vc_toolkit()

if __name__ == "__main__":
    uvicorn.run(vc_toolkit, host="0.0.0.0", port=8080)
