LEGAL_PARTICIPANT = {
    "@context": [
        "https://www.w3.org/ns/credentials/v2",
        "https://w3id.org/security/suites/jws-2020/v1",
        "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
    ],
    "type": "VerifiableCredential",
    "id": '',
    "issuer": '',
    "credentialSubject": {
        "id": '',
        "gx:legalName": '',
        "gx:headquarterAddress": {
            "gx:countrySubdivisionCode": ''
        },
        "gx:legalRegistrationNumber": {
            "id": ''
        },
        "gx:legalAddress": {
            "gx:countrySubdivisionCode": ''
        },
        "type": "gx:LegalParticipant"
    }
}

SERVICE_OFFERING = {
    "@context": [
        "https://www.w3.org/ns/credentials/v2",
        "https://w3id.org/security/suites/jws-2020/v1",
        "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
    ],
    "type": "VerifiableCredential",
    "id": "",
    "issuer": "",
    "credentialSubject": {
        "id": "",
        "type": "gx:ServiceOffering",
        "gx:providedBy": {
            "id": "",
        },
        "gx:name": "",
        "gx:serviceOffering:serviceEndpoint": "",
        "gx:termsAndConditions": {
        "gx:URL": "",
        "gx:hash": ""
        },
        "gx:policy": "",
        "gx:dataAccountExport": {
        "gx:requestType": "API",
        "gx:accessType": "digital",
        "gx:formatType": "application/json"
        }      
    }
}