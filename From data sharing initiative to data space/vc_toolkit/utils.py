from hashlib import sha256

import jcs
from jwcrypto import jws
from jwcrypto.common import json_encode
from pyld import jsonld
import os
import io
import json
import requests
import datetime


def compact_token(token):
    parts = token.split(".")
    return parts[0] + ".." + parts[2]


def normalize(doc):
    return jsonld.normalize(doc, {'algorithm': 'URDNA2015', 'format': 'application/n-quads'})


def sha256_normalized_vc(normalized_vc):
    return sha256(normalized_vc.encode('utf-8'))


def sha256_string(canonized_vc):
    return sha256(canonized_vc).hexdigest()


def canonicalize(doc):
    return jcs.canonicalize(doc)


def sign_doc(doc, private_key, issuer_verification_method):
    doc["validFrom"] = datetime.datetime.now(datetime.timezone.utc).replace(microsecond=0).isoformat()
    doc["validUntil"] = (datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(days=90)).replace(microsecond=0).isoformat()
    
    # URDNA normalize
    normalized = normalize(doc)
    # sha256 the RDF
    normalized_hash = sha256_normalized_vc(normalized)
    # Sign using JWS
    jws_token = jws.JWS(normalized_hash.hexdigest())
    jws_token.add_signature(private_key, None,
                            json_encode({"alg": "PS256", "b64": False, "crit": ["b64"]}),
                            json_encode({"kid": private_key.thumbprint()}))
    signed = jws_token.serialize(compact=True)
    doc['proof'] = {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": issuer_verification_method,
        "jws": compact_token(signed)
    }
    return doc


def upload_doc(host_uri: str, host_api_key: str, doc: dict):
    """

    """
    name = os.path.basename(doc['id'])
    print("saving file {} at {}".format(name, doc['id']))

    payload = {}
    data = io.BytesIO(str.encode(json.dumps(doc)))
    files = [('file', (name, data, 'application/json'))]

    headers = {'x-api-key': host_api_key}
    response = requests.request("POST", host_uri, headers=headers, data=payload, files=files)
    print("Response: " + response.text)