using GaiaXCredentialGenerator.Authentication;
using Microsoft.AspNetCore.Mvc;

namespace GaiaXCredentialGenerator.Controllers
{
    [ApiKey]
    [ApiController]
    [Route("[controller]")]
    public class FileController : ControllerBase
    {
        private readonly ILogger<FileController> _logger;

        public FileController(ILogger<FileController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromQuery] string folder, [FromForm] IFormFile file)
        {
            if (file.Length > 0)
            {
                var outfolder = Path.Combine("wwwroot", folder);
                if (!Path.Exists(outfolder))
                {
                    return NotFound($"folder {folder} does not exist");
                }
                var filePath = Path.Combine(outfolder, file.FileName);

                using (var stream = System.IO.File.Create(filePath))
                {
                    await file.CopyToAsync(stream);
                }
            }
            return Ok(file.FileName);
        }
    }
}
